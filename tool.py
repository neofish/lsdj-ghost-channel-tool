""" 20171207 Sam Fisk
"""

import click
from pprint import pprint
import pylsdj
import math
from itertools import chain
from copy import copy

from configurer import configurer


# Config options to extract and parse.
config_options = {
  # Input section.
  'wave-data': {
    'form': (list, list, str), 'parsing': 'hex-to-list' },
  'load-waves-from-lsdsng': {
    'form': str, 'parsing': 'file-exists' },
  'lsdsng-wave-ranges': {
    'form': (list, list, str), 'parsing': 'hex-to-int' },
  # Processing section.
  'time-mod-length': {
    'form': str, 'default': False, 'parsing': 'hex-to-int' },
  'time-mod-method': {
    'form': (list, str), 'default': 'loop', 'parsing': ['once','loop','pingpong'] },
  'volume-levels': {
    'form': (list, list, str), 'parsing': 'hex-to-int' },
  'volume-pre-mod-method': {
    'form': str, 'default': 'scale' },
  'volume-mod-method': {
    'form': (list, str), 'default': 'scale', 'parsing': 'hex-or-scale' },
  'underflow-method': {
    'form': str, 'default': 'clip', 'parsing': ['clip','wrap'] },
  'overflow-method': {
    'form': str, 'default': 'clip', 'parsing': ['clip','wrap'] },
  'octave-count': {
    'form': (list, str), 'default': 1, 'parsing': 'hex-to-int' },
  'octave-mod-method': {
    'form': (list, str), 'default': 'interpolate', 'parsing': ['interpolate','truncate'] },
  # Output section.
  'wave-table-offset': {
    'form': str, 'default': 0, 'parsing': 'hex-to-int' },
  'warn-on-overflow': {
    'form': bool, 'default': True },
  'save-graphs': {
    'form': bool, 'default': False },
  'save-to-stdout': {
    'form': bool },
  'save-to-txt': {
    'form': str, 'parsing': 'file' },
  'save-to-lsdsng': {
    'form': str, 'parsing': 'file' },
  'lsdsng-song-name': {
    'form': str, 'default': 'ghost' }
}
# Config requirements.
config_requirements = [
  ('wave-data', # Need Wave data specified OR from lsdsng.
    ['load-waves-from-lsdsng','lsdsng-wave-ranges']),
  'volume-levels', # Need levels.
  ('save-to-stdout','save-to-txt','save-to-lsdsng') # Need an output method.
]

def hex_(value):
  """Returns the built-in value of hex, with the 0x stripped off.
  """
  if value < 0:
    return '-'+hex(value)[3:]
  return hex(value)[2:]

def get_wave_data_from_lsdsng(config):
  """Either loads wave data from specified places in an lsdsng.
  Not reporting errors fully because most should be handled by configurer.
  """
  # See if the load from file property exists.
  key = 'load-waves-from-lsdsng'
  if key not in config:
    return False
  load_from_lsdsng = config[key]
  # See if the value is sane (might have been wiped out by parser).
  if type(load_from_lsdsng) is not str:
    return False
  filename = load_from_lsdsng
  lsdsng = pylsdj.load_lsdsng(filename)
  # TODO Check this worked. print('!!! Cannot load song.')
  print('Loaded song: {} ({}).'.format(
    lsdsng.name.decode('utf-8'),
    hex_(lsdsng.version)))
  # See if the waves have been specified.
  key = 'lsdsng-wave-ranges'
  if key not in config:
    return False
  wave_ranges = config[key]
  # Check it's sane again.
  if type(wave_ranges) is not list:
    return False
  # Prepare output array.
  waves = []
  # Iterate the list.
  for wave_range in wave_ranges:
    # Check it's sane again.
    if type(wave_range) is not list:
      return False
    # Prepare storage for wave.
    wave_data = []
    # Get start and end waves.
    start = wave_range[0]
    end = wave_range[1]
    length = end-start+1
    #print('Loading wave from {} to {} ({} waves)'.format(hex_(start),hex_(end),hex(length)))
    # Iterate length.
    current = start
    for i in range(length):
      # Find the synth current belongs to (round to nearest 16 and divide by 16).
      synth_num = math.floor(current / 16)
      # Find the wave to access.
      wave_num = current - (synth_num*16)
      #print('Accessing synth {}, wave {}'.format(hex_(synth_num),hex_(wave_num)))
      # Access the synth and wave table.
      wave = list(lsdsng.song.synths[synth_num].waves[wave_num])
      #print(wave)
      wave_data.append(wave)
      # Increment.
      current = current + 1
    # Append the compiled data.
    waves.append(wave_data)
  #print(wave_data_to_string(waves,config,print_waveform=True))
  return waves


def get_wave_data(config):
  """Either loads wave data from specified places in an lsdsng or pulls it
  straight from the config.
  """
  # Try from lsdsng.
  data_from_lsdsng = get_wave_data_from_lsdsng(config)
  if type(data_from_lsdsng) is list:
    return data_from_lsdsng
  # Try from config.
  key = 'wave-data'
  if key in config:
    return config[key]
  # Failed.
  return False

def save_to_lsdsng(wave_data, config):
  """Loads a blank lsdsng and saves to it.
  """
  key = 'save-to-lsdsng'
  save_to_lsdsng = config[key] if key in config else False
  if save_to_lsdsng is not False:
    # Load blank song.
    song = pylsdj.load_lsdsng('blank_song.lsdsng')
    # Set the name.
    song.name = config['lsdsng-song-name'].upper()
    # Copy data in.
    wave_i = config['wave-table-offset']
    print(wave_i)
    for wave in wave_data[0]:
      # Access relevant synth.
      synth_i = int(wave_i/16)
      synth = song.song.synths[synth_i]
      waves_i = wave_i%16
      #print(synth_i,waves_i)
      for i, wave_frame in enumerate(wave):
        synth.waves[waves_i][i] = wave_frame
      wave_i = wave_i + 1
    # Save!
    song.save(save_to_lsdsng)
    print('Saved lsdsng to {}.'.format(save_to_lsdsng))

def wave_to_string(wave, dest=None, comment=None, print_waveform=False):
  """Prints a wave with optional destination wavetable info, a comment,
  and a primitive ASCII waveform.
  """
  output = ''
  dest_comment_format = '{}\t{}'.format(
    dest if dest is not None else '',
    comment if comment is not None else '')
  if dest is not None or comment is not None:
    output += dest_comment_format + '\r\n'
  wavestr = ''.join([hex_(v) for v in wave])
  output += wavestr[:16] + '\r\n'
  if print_waveform:
    for i in range(16):
      out = ''
      for inst in wave:
        new_ch = ' '
        if inst == 15-i:
          new_ch = u'\u2588'
        out += new_ch
      output += out + '\r\n'
    output += ' '*16+wavestr[16:] + '\r\n'
  else:
    output += wavestr[16:] + '\r\n'
  output += '\r\n'
  return output

def wave_data_to_string(wave_data, config, print_waveform=False, comments=None):
  """Prints all the waves contained in a wave_data list, using
  config['wave-table-offset'] to offset the tacos.
  """
  output = ''
  main_wave_format = 'Main wave'
  sub_wave_format = 'Sub wave {}'
  wave_start_counter = config['wave-table-offset']
  wave_counter = wave_start_counter
  for i, wave_group in enumerate(wave_data):
    comment = sub_wave_format.format(i-1) if i > 0 else main_wave_format
    for j, wave in enumerate(wave_group):
      if comments is not None:
        comment = comments[j]
      dest = hex_(wave_counter)
      output += wave_to_string(wave,dest,comment,print_waveform)
      wave_counter = wave_counter + 1
    wave_start_counter = wave_start_counter + 16
    wave_counter = wave_start_counter
  return output

def process_waves(wave_data, config):
  """Processes all the wave data...
  """
  # Collate all variables.
  time_mod_length = config['time-mod-length']
  time_mod_method = config['time-mod-method'] # Includes main.
  volume_levels = config['volume-levels'] # Includes main.
  volume_pre_mod_method = config['volume-pre-mod-method']
  volume_mod_method = config['volume-mod-method']
  underflow_method = config['underflow-method']
  overflow_method = config['overflow-method']
  octave_count = config['octave-count']
  octave_mod_method = config['octave-mod-method']
  # Adjust variables if needed (mostly because configurator can't look ahead to this info).
  if time_mod_length is False: # Find the longest length wave.
    time_mod_length = max([len(w) for w in wave_data])
  if type(time_mod_method) is not list:
    time_mod_method = [config_options['time-mod-method']['default']] * len(wave_data)
  if type(volume_mod_method) is not list:
    volume_mod_method = [config_options['volume-mod-method']['default']] * len(wave_data)
  if type(octave_count) is not list:
    octave_count = [config_options['octave-count']['default']] * len(wave_data)
  if type(octave_mod_method) is not list:
    octave_mod_method = [config_options['octave-mod-method']['default']] * len(wave_data)
  # Pre-mod volume (scale to max volume).
  if volume_pre_mod_method == 'scale':
    for i, waves in enumerate(wave_data):
      # Get the max allowed volume.
      max_allowed_volume = volume_levels[i][1]
      #print(max_allowed_volume,type(max_allowed_volume))
      # Get the max volume across all waves in the group.
      max_actual_volume = max(chain.from_iterable(waves))
      #print('Max vol: {}'.format(max_actual_volume))
      # Only bother scaling if the max isn't too big.
      if max_actual_volume <= max_allowed_volume:
        continue
      # Calc the scaling factor
      scaling_factor = max_allowed_volume / max_actual_volume
      #print('scaling factor: {}'.format(scaling_factor))
      # Scale the volumes.
      wave_data[i] = [[round(v*scaling_factor) for v in w] for w in waves]
  # Mod time (match lengths).
  for i, waves in enumerate(wave_data):
    # Check to see if the wave group is long enough.
    waves_len = len(waves)
    length_missing = time_mod_length - waves_len
    #print('len missing',length_missing)
    #pprint([''.join([str(v) for v in w]) for w in wave_data[i]])
    if length_missing > 0:
      # Fill it in with various methods.
      # once (blank out the rest).
      if time_mod_method[i] == 'once':
        wave_data[i] += length_missing * [32 * [0]]
        #print(wave_data[i])
      # loop (repeat existing form to fill).
      elif time_mod_method[i] == 'loop':
        while length_missing > 0:
          new_part = []
          if waves_len > length_missing:
            new_part = waves[:length_missing]
          else:
            new_part = waves
          wave_data[i] += new_part
          length_missing = time_mod_length - len(wave_data[i])
      # pingpong (repeat existing form backwards then forwards to fill).
      elif time_mod_method[i] == 'pingpong':
        ping = False
        while length_missing > 0:
          if waves_len > length_missing:
            new_part = waves[:length_missing]
          else:
            new_part = waves
          if not ping:
            new_part = copy(new_part)
            new_part.reverse()
          wave_data[i] += new_part
          ping = not ping
          length_missing = time_mod_length - len(wave_data[i])
    #print(wave_data[i])
    #pprint([''.join([str(v) for v in w]) for w in wave_data[i]])
  # Create sub-wave storage.
  sub_wave_data = wave_data[1:]
    # Make space for the sequence of sub_waves.
  sub_waves_out = [0]*len(sub_wave_data)
  # Iterate sub-waves.
  for i, sub_waves in enumerate(sub_wave_data):
    # Make space for the length of the wave frame.
    sub_waves_out[i] = [0]*len(sub_waves)
    # Iterate length.
    for j, sub_wave in enumerate(sub_waves):
      # Create space for octaves.
      sub_waves_out[i][j] = [0]*octave_count[i]
      # Iterate octaves (starting at no change).
      for o in range(octave_count[i]):
        # Create octave-modded wave.
        octave_modded_wave = copy(sub_wave)
        #print('subwave {} frame {} octave {}'.format(i,j,o))
        if o > 0:
          # Convert o to power of two.
          o2 = 1 << o
          # Interpolate.
          if octave_mod_method[i] == 'interpolate':
            # Take average of every o2 points.
            averages = []
            for group in [sub_wave[k:k+o2] for k in range(0,len(sub_wave),o2)]:
              average = round(sum(group) / o2)
              averages.append(average)
            # Paste o2 times.
            octave_modded_wave = averages * o2
          # Truncate.
          elif octave_mod_method[i] == 'truncate':
            # Take first 1/o2 of wave form.
            points_to_take = round(len(sub_wave)/o2)
            # Paste o2 times.
            #print('Pasting {} frames {} times.'.format(points_to_take,o2))
            copied_wave = sub_wave[:points_to_take]
            octave_modded_wave = copied_wave * o2
          else:
            print('!!! Sub wave octave error.')
        #print(wave_to_string(octave_modded_wave,print_waveform=True))
        # Create space for volumes.
        min_vol, max_vol = volume_levels[i+1]
        vol_range = max_vol-min_vol+1
        #print(vol_range)
        sub_waves_out[i][j][o] = [0]*vol_range
        #print(sub_waves_out)
        # Iterate volumes (starting at no change).
        for vi, v in enumerate(range(max_vol,min_vol-1,-1)):
          #print('subwave {} frame {} octave {} volume {}'.format(i,j,o,v))
          # Create volume modded wave.
          volume_modded_wave = copy(octave_modded_wave)
          # Scale volume.
          if volume_mod_method[i+1] == 'scale':
            scaling_factor = v / max_vol
            volume_modded_wave = [round(vv*scaling_factor) for vv in volume_modded_wave]
          # Fixed reduction in volume.
          elif type(volume_mod_method[i+1]) is int:
            volume_reduction = volume_mod_method * vi
            volume_modded_wave = [vv - vi for vv in volume_modded_wave]
          else:
            print('!!! Sub wave volume error: {} [{}] {}.'.format(volume_mod_method[i+1],vi,v))
          #print_wave(volume_modded_wave,print_waveform=True)
          # Apply underflow and overflow.
          for vvi, vv in enumerate(volume_modded_wave):
            if vv > 15:
              if overflow_method == 'clip':
                vv = 15
              elif overflow_method == 'wrap':
                vv = vv % 16
            if vv < 0:
              if overflow_method == 'clip':
                vv = 0
              elif overflow_method == 'wrap':
                vv = vv % 16
            volume_modded_wave[vvi] = vv
          # Store wave.
          #print('{} {} {} {}'.format(i,j,o,vi))
          sub_waves_out[i][j][o][vi] = volume_modded_wave
  # Create output.
  output = []
  # Prepare main wave.
  main_wave = wave_data[0]
  # Prepare array of comments to complement waves.
  comments = []
  comment_main_format = 'Frame {} Main (Vol {})'
  comment_sub_format = 'Sub {} (Oct {}, Vol {})'
  # Prepare max iteration depths for each property.
  max_octaves = max(octave_count)
  max_volumes = max([v[1]-v[0]+1 for v in volume_levels[1:]])
  # Iterate each subwave volume.
  for vi in range(max_volumes):
    # Iterate each subwave octave:
    for oi in range(max_octaves):
      # Iterate length.
      for li in range(len(main_wave)):
        # Modulate volume of main wave.
        main_wave_vol_min, main_wave_vol_max = volume_levels[0]
        for mvi, mv in enumerate(range(main_wave_vol_max,main_wave_vol_min-1,-1)):
          #print(mvi,mv)
          # Copy main wave.
          working_wave = copy(main_wave[li])
          # Scale volume.
          if volume_mod_method[0] == 'scale':
            scaling_factor = mv / main_wave_vol_max
            #print(scaling_factor)
            working_wave = [round(mvv*scaling_factor) for mvv in working_wave]
          # Fixed reduction in volume.
          elif type(volume_mod_method[0]) is int:
            volume_reduction = volume_mod_method[0] * mvi
            working_wave = [mvv - mvi for mvv in working_wave]
          else:
            print('!!! Main wave volume error: {} [{}] {}.'.format(
              volume_mod_method[0],mvi,mv))
          # Create comment for main wave.
          comment_main = comment_main_format.format(hex_(li),hex_(mv))
          # Prepare comments for sub waves.
          comment_subs = []          
          # Iterate sub-waves.
          for si in range(len(sub_waves_out)):
            # Access correct wave form for sub-wave.
            #print(si,li,oi,vi)
            sub_wave = sub_waves_out[si][li][oi][vi]
            #print(si,li,oi,vi,sub_wave)
            # Add it to working wave.
            for ii in range(len(working_wave)):
              before = working_wave[ii]
              working_wave[ii] = working_wave[ii] + sub_wave[ii]
              #print(before,sub_wave[ii],working_wave[ii])
            # Apply underflow and overflow.
            for vvi, vv in enumerate(working_wave):
              if vv > 15:
                if overflow_method == 'clip':
                  vv = 15
                elif overflow_method == 'wrap':
                  vv = vv % 16
              if vv < 0:
                if overflow_method == 'clip':
                  vv = 0
                elif overflow_method == 'wrap':
                  vv = vv % 16
              working_wave[vvi] = vv
            # Prepare comment for subwave.
            comment_sub = comment_sub_format.format(
              hex_(si),hex_(oi),hex_(volume_levels[si+1][1]-vi))
            comment_subs.append(comment_sub)
            # Prepare comments for subwave.
            comment_subs = ' '.join(comment_subs)
            # Store wave.
            output.append(working_wave)
            # Store comment
            comments.append('{} {}'.format(comment_main,comment_subs))
            comment_subs = []
  # Done.
  return [output], comments

@click.command()
@click.argument('config-filename', type=click.Path(exists=True))
def run_tool(config_filename):
  """Tool to take 2 or more distinct waveforms and transform them into a set of
  merged waveforms, allowing for time-, volume-, and octave- modulations of the
  input waveforms, with some handy features. Uses pylsdj for IO with LSDJ saves.
  Read README.md and example.conf for information.
  """
  # Load and parse the config.
  config, success = configurer(config_filename,config_options,config_requirements)
  #print(config)
  if not success:
    print('!!! Parsing config failed, please address errors. Early exit...')
    return
  # Load wave data from lsdsng or pull straight from config.
  wave_data = get_wave_data(config)
  if type(wave_data) is not list:
    print('!!! Could not get wave data.')
    return
  # Process waves.
  wave_data, comments = process_waves(wave_data,config)
  # Print/save to stdout and/or txt file.
  key = 'save-to-stdout'
  save_to_stdout = config[key] if key in config else False
  key = 'save-to-txt'
  save_to_txt = config[key] if key in config else False
  if save_to_stdout or save_to_txt is not False:
    wave_data_str = wave_data_to_string(wave_data,config,print_waveform=config['save-graphs'],comments=comments)
    if save_to_stdout:
      print(wave_data_str)
      print('Total wave frames out: {}'.format(len(wave_data[0])))
    if save_to_txt is not False:
      with open(save_to_txt,'w') as txt:
        txt.write(wave_data_str)
      print('Saved txt to {}.'.format(save_to_txt))
  # Save to lsdsng.
  save_to_lsdsng(wave_data,config)

if __name__ == '__main__':
  run_tool()
