lsdj-ghost-channel-tool
===
Python 3 project to take 2 or more distinct waveforms and transform them into a set of merged waveforms, allowing for time-, volume-, and octave- modulations of the input waveforms, with some handy features. Uses pylsdj for IO with LSDJ saves.

Lazy license terms
---
- You can copy/distribute/edit this work for free.
- You must give credit (link to this).
- If fixing bugs or adding minor features, please submit a pull request and I'll credit you.

Links
---
- Frame calculator - to be completed, version that works with limitations: https://docs.google.com/spreadsheets/d/1BxuqpXMj1Qmm-ega--HeZYmC6xQo0sX_KZFquir4hTA/edit?usp=sharing
- pylsdj - https://github.com/alexras/pylsdj
- Ghost channel tutorial - https://www.youtube.com/watch?v=73VLMNMaRAQ

Detailed description
===

TODO

Installation / HOWTO
===

- Install Python 3.
- Install jsmin and pylsdj: `pip install jsmin pylsdj`
- Clone this repo: `git clone git@gitlab.com:neofish/lsdj-ghost-channel-tool.git`

- `python tool.py conf/tacos.json`

Reference
===

Read the examples in `conf/`. :)
