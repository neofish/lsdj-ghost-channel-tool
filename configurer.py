""" 20171207 Sam Fisk
Magic configurer that handles some defaults and structure so parsing/validation
funcs can be run. Also handles required fields.
"""

import json
from jsmin import jsmin
import os

# FORMATTING
# JSON.
# All numbers must be given in hexadecimal strings, but can be in capital or
# lower case.
# Hex string options are stripped of all whitespace, so can use it liberally.

def configurer(filename, options, requirements):
  """Loads a file and parses it for config options, using the rules above,
  and looking for things specified in options. Reqs defines if to through errors.
  """
  # Open file and strip comments.
  with open(filename) as file:
    file_no_comments = jsmin(file.read())
  # Parse JSON.
  try:
    conf_in = json.loads(file_no_comments)
  except json.decoder.JSONDecodeError as err:
    print('JSON as seen by decoder:')
    print(file_no_comments)
    raise err
  # Create output object. 
  conf_out = {}
  # Iterate options.
  for name, settings in options.items():
    # Set default (if there is one).
    def_str = 'default'
    if def_str in settings:
      conf_out[name] = settings['default']
    # Look for option in json.
    if name in conf_in:
      conf_option = conf_in[name]
      #print(name)
      #print(settings)
      #print(conf_option)
      # Fetch the form info and transform to iterable.
      form = settings['form']
      if type(form) is tuple:
        form = list(form)
      else:
        form = [form]
      #print(form)
      # Function to recursively traverse iterable form info, checking for validation errors.
      def validate_form(conf_option, form):
        # Check root type.
        if type(conf_option) is not form[0] and conf_option is not False:
          return '{} is {}, not {}'.format(conf_option,type(conf_option),form[0])
        # If list, iterate it, checking nodes.
        if form[0] is list:
          reduced_form = form[1:]
          for i, v in enumerate(conf_option):
            result = validate_form(v,reduced_form) 
            if result is not True:
              return '{} > {}'.format(i,result)
        return True
      # Validate the conf.
      result = validate_form(conf_option,form)
      if result is not True:
        print('!!! Validation failed, using default and continuing: {} > {}.'.format(name,result))
        continue
      # Run parsing code (if there is any). Only applies to strings.
      par_str = 'parsing'
      if par_str in settings:
        parsing = settings[par_str]
        # Function to find all string components of conf_option and run a parsing
        # func on them.
        def run_parsing(conf_option, parsing):
          # Get the type of the item.
          conf_opt_type = type(conf_option)
          # If it's a string, parse it.
          if conf_opt_type is str:
            return parse(conf_option,parsing)
          # If it's a list, iterate it and collate result.
          if conf_opt_type is list:
            return [run_parsing(v,parsing) for v in conf_option]
          # Otherwise return.
          return conf_option
        # Run the parsing code.
        conf_option = run_parsing(conf_option,parsing)
      # Set the value.
      conf_out[name] = conf_option
    #print()
  # Parse requirements. [] is AND, () is OR.
  def parse_reqs(reqs):
    # Handle lists (AND).
    if type(reqs) is list:
      for req in reqs:
        if parse_reqs(req) == False:
          print('!!! Did not satisfy requirement of needing: {}'.format(req))
          return False
      return True
    # Handle tuples (OR).
    if type(reqs) is tuple:
      for req in reqs:
        if parse_reqs(req) == True:
          return True
      print('!!! Did not satisfy requirements of needing one of: {}'.format(reqs))
      return False
    # Handle keys to check config for.
    return reqs in conf_out
  success = parse_reqs(requirements)
  return conf_out, success


def parse(value, method):
  """Used to parse inputs from config. Hex, or in a list of values, or a file, etc.
  """
  #print('Parse: ',value,method)
  # Parse lists.
  if type(method) is list:
    if value not in method:
      print('!!! Parse failed: {} (must be {}).'.format(value,method))
      return False
    return value
  # Hex-or-scale (because I'm lazy).
  if method == 'hex-or-scale':
    if value == 'scale':
      return value
  # Hex-to-int.
  if method == 'hex-or-scale' or method == 'hex-to-int':
    # Strip all whitespace.
    value = ''.join(value.split())
    # Convert from hex.
    value = int(value,16)
    #print(value)
    return value
  # Hex-to-list.
  if method == 'hex-to-list':
    # Strip all whitespace.
    value = ''.join(value.split())
    #print(value)
    # Convert to list and from hex.
    value = [int(v,16) for v in list(value)]
    #print(type(value[0]),value)
    return value
  # File.
  if method == 'file':
    return os.path.abspath(value)
  # File-exists.
  if method == 'file-exists':
    path = os.path.abspath(value)
    #print('Filepath:',path)
    if not os.path.exists(path) or not os.path.isfile(path):
      print('!!! Parse failed: {} (must be {}).'.format(value,method))
      return False
    return path
